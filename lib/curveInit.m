function curve = curveInit(curve,functionName,inputs)
% CURVEINIT(CURVE)  
%   Initialize CURVE. Init CURVE.SYS structure if it does not exist.

if nargin < 1
    curve = struct;
end

if nargin < 2
    functionName = '';
end

if nargin < 3
    inputs = {};
end

if ~isfield(curve,'sys')
    sys.startTime       = [];
    sys.elapsedTime     = 0;
    sys.functionName    = functionName;
    sys.workInterval    = 1200;
    sys.inputs          = inputs;
    
    sys.maxWorkers      = 1;     %requested workers
    sys.parallelPool    = [];    %gcp 
    sys.numberOfWorkers = [];    %number of workers provided
    sys.equalField      = {};    %how to combine fields after parallel processing
    sys.addField        = {};
    sys.concatField     = {};
    sys.firstField      = {};
    
    for ii = 1:size(curve,1)
        for jj = 1:size(curve,2)
            curve(ii,jj).sys = sys;
        end
    end
    
end
