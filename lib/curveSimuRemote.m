function curveSimuRemote(inputString,jobname)
%curveSimuRemote - Run a simulation on a remote server
%
%   This function requires configuration variables in PBS/CONFIGUREREMOTE.m
%   are set properly.
%
%   On Mac and Linux systems, CURVESIMUREMOTE will upload files in the
%   current directory to the remote server, and runs the specified program.
%   Command-line utilties rsync and ssh must be present. On Windows systems,
%   the user must manually upload files, and remote execution requires
%   putty.exe.
%
%   There are two operation modes.  In both modes, the directory tree is
%   copied to the remote server.
%
%   CURVESIMUREMOTE('simufile')  If SIMUFILE.MAT exists in the current
%   directory, and contains a structure CURVE, then the remote Matlab
%   command is curveSimu('simufile'), running the simulation.
%
%   CURVESIMUREMOTE('command')  If the first argument is not a MAT file,
%   then it is treated as a Matlab command to be run on the remote server.
%   This mode does not use the Curve Library, but is a convenient way to
%   remotely execute Matlab commands. For example, the following computes
%   the eigenvalues of a random matrix, and saves the result to a file.
%
%      cmd = '[V,D] = eig(rand(100)); save mydata V D;';
%      curveSimuRemote(cmd);
%
%   Since simulations nomially save data to files on the remote server,
%   these files are copied to the local machine.
%
%   curveSimuRemote(...,jobname) changes the job name from default of
%   'userjob' to jobname.  This is useful when running multiple jobs in
%   the same directory.
%
%   See also CURVEFROMREMOTE
%
%   Brian Kurkoski 2016


if nargin < 1
    error('requires one input argument')
end

if nargin < 2
    jobname = 'userjob';
end


try
    %input is a MAT filename
    loadFile = true;
    H = load(inputString);
catch
    warning('Input is interpreted as a Matlab command');
    loadFile = false;
end

if loadFile
    fprintf('Input is a MAT file\n')
    assert(isfield(H,'curve'),'File %s.MAT does not contain CURVE\n',upper(inputString));
    assert(isstruct(H.curve),'CURVE in file %s.MAT is not a structure\n',upper(inputString));
    remoteMatlabCommand = sprintf('curveSimu(''%s'');',inputString);
else
    fprintf('Input is a Matlab command\n')
    remoteMatlabCommand = inputString;
end

% d = dir;
% t = strcmpi({d.name},[inputString '.mat']);
% if any(t)
%     %input is a MAT filename
%     H = load(inputString);
%     assert(isfield(H,'curve'),'File %s.MAT does not contain CURVE\n',upper(inputString));
%     t = H.curve;
%     assert(isstruct(H.curve),'CURVE in file %s.MAT is not a structure\n',upper(inputString));
%     
%     remoteMatlabCommand = sprintf('curveSimu(''%s'');',inputString);
% else
%     %input is a Matlab command
%     warning('Input is interpreted as a Matlab command');
%     remoteMatlabCommand = inputString;
%     keyboard
% end



[config,result,pbsdir,configurationFileName] = curveRemoteLoadConfiguration;

if ~result
    fprintf('Failed to load configuration file: %s/%s must be a configuration file\n',pbsdir,configurationFileName);
    return
end

pbsfilename = [ pbsdir '/'  jobname  '.pbs'];
FL = fopen(pbsfilename,'w');
fprintf(FL,'#!/bin/sh\n\n');
fprintf(FL,'#PBS -l select=1\n');

if isfield(config,'pbsQueueClass')
    fprintf(FL,'#PBS -q %s\n',config.pbsQueueClass);
end

fprintf(FL,'#PBS -j oe\n');
fprintf(FL,'#PBS -e %s/%s.ER\n',pbsdir,jobname);
fprintf(FL,'#PBS -o %s/%s.OU\n',pbsdir,jobname);
fprintf(FL,'#PBS -N %s\n',jobname);
fprintf(FL,'cd $PBS_O_WORKDIR\n');
fprintf(FL,'export MATLABROOT=/opt/MATLAB\n');
fprintf(FL,'export PATH="$PATH:$MATLABROOT/bin"\n');
%fprintf(FL,'#export MATLABPATH=$PBS_O_WORKDIR\n\n');
%fprintf(FL,'#export MATLAB_USE_USERWORK=1\n');
fprintf(FL,'matlab -nodisplay -r "%s" \n',remoteMatlabCommand);
fclose(FL);
fprintf('Created file %s\n',upper(pbsfilename));


%copy additional files
if ~ispc
    if isfield(config,'shellCommand')
        error('shellCommand was changed to localShellCommand.  Update configureRemote.m');
    end
    if isfield(config,'localShellCommand') && ~isempty(config.localShellCommand)
        fprintf('Local shell command in config file\n');
        %fprintf('Shell command: %s\n',config.shellCommand);
        status = system(config.localShellCommand);
        if status
            error('Shell command on local machine failed');
        end
    end
end

%rsync files to host
if ~ispc
    fprintf('Rsync files to %s\n',upper(config.servername));
    %--update is important if a simulation is already running in same dir
    cmd    = sprintf('rsync -ae ssh --update . %s@%s:%s',config.username,config.servername,config.remoteDir);
    status = system(cmd);
    if status
        error('rsync to server failed');
    end
else
    fprintf('WINDOWS: Copy files to server manually.  When done,\n')
    reply = input('run command on server? y/n [y]:','s');
    if isempty(reply)
        reply = 'y';
    end
    if ~strcmpi(reply,'y')
        fprintf('Do nothing\n')
        return
    end
end


%qsub to submit PBS job

if ~ispc
    if config.usePBS
        fprintf('Submit PBS job: %%qsub pbs/%s.pbs\n',upper(jobname));
        cmd = sprintf('ssh %s@%s "cd %s ; qsub %s/%s.pbs"',config.username,config.servername,config.remoteDir,pbsdir,jobname);
    else
        fprintf('Run MATLAB direct\n');
        %some versions require "source ~/.bashrc ; source ~/.bash_profile"
        %THIS IS AN EXAMPLE OF THE REMOTE COMMAND WE WANT TO EXECUTE
        %ssh kurkoski@150.65.124.11 "source ~/.bashrc ; source ~/.bash_profile ; cd ~/myProject ; LANG=en_GB.utf-8 ; matlab -nodisplay -r 'disp(pwd) ; disp(date)'   > var/userjob.out 2> var/userjob.err & "
        %THIS IS A SIMPLER VERSION
        %ssh kurkoski@150.65.124.11 "cd ~/myProject ; matlab -nodisplay -r 'disp(pwd) ; disp(date)' > var/userjob.out 2> var/userjob.err & "
        cmd = sprintf('ssh %s@%s "cd %s ; matlab -nodisplay -r ''%s'' > var/%s.out 2> var/%s.out & "',...
            config.username,config.servername,config.remoteDir,remoteMatlabCommand,jobname,jobname);
    end
    cmd 
    [status,result] = system(cmd);
    if status
        error('submit PBS job failed');
    else
        jobID = regexp(result,'\d+','match');
    end
else
    FL = fopen('servernameCommands','w')
    if config.usePBS
        %fprintf('Submit PBS job: %%qsub var/%s.pbs',upper(jobname));
        fprintf(FL,'"cd %s ; qsub var/%s.pbs"\n',config.remoteDir,jobname);
    else
        %other candidates
        % old: matlab -nojvm < G3_n100_d3.m > stdout2.txt 2> stderr2.txt &
        % old: matlab  -nodesktop -nodisplay -r "simu('data/G3_n100d5_longer');" &
        % old: matlab -nojvm -r "simu('data/G3_n100d5_longer');" & > stdout.txt 2> stderr.txt
        fprintf('Run MATLAB direct\n');
        fprintf(FL,'cd %s ; matlab -nodisplay -r ''%s'' > var/%s.out 2> var/%s.out & \n',...
            config.remoteDir,remoteMatlabCommand,jobname,jobname);
    end
    
    fclose(FL);
    
    cmd = sprintf('%s %s@%s -m servernameCommands',config.puttyPath,config.username,config.servername);
    [status,result] = system(cmd);
    if status
        error('submit PBS job failed');
    else
        jobID = regexp(result,'\d+','match');
    end
end

if ~isempty(jobID) 
    if nargout == 0
        fprintf('Job ID is %s. Useful commands:\n >> curveRemoteShell(''qstat %s'') %%get status on server\n >> curveFromRemote  %%copy files from server to local\n >> curveRemoteShell(''qdel %s'')   %%delete job\n >> curveRemoteShell(''qstat -u username'') %%jobs owned by username\n',jobID{1},jobID{1},jobID{1});
    end
end

%if possible, get output which includes job number.
%     printf("line 0 : $output[0]\n");
%     preg_match("/[^\d]{0,}(\d*)/", $output[0] , $matches);
%     printf("Job number: $matches[1]");
%useful?
%runphpFile = mfilename('fullpath');

return



