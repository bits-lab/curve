function curveThresholdSearch(filename)


% curve.searchParam is the curve parameter to be searched, for example
%   curve.searchParam = 'SNRdB' means there is a parameter curve.SNRdB.
%
% curve.ilow and curve.ihigh contains the initial high and low parameter values used
% to start the search
%
% curve.resolution  The search continues until the gap between high and low
% is less than curve.resolution.
%
% curve.check is an output of the function, and is either 0 or 1.
% Your function should set check = 0 to indicate the search parameter value is
% less than the threshold, and should set check = 1 to indicate the search parameter 
% value is greater than the threshold.  Note that if SNR is being searched for a noise
% threshold, then check = 0 means non-convergence, since a low SNR suggests
% non-convergence.  On the other hand, if noise variance is being searched
% (which is the reciprocal of SNR), then check = 1 means non-convergence,
% since high noise variance suggests non-convergence.

%curveGenerate('thresholdResults','ldpc_de','param','SNR','ilow',0,'ihigh',1)

load(filename)


%initialize
if length(curve) == 1
    %get the initial values for the threhold search parameters
    %(use of thresholdInitReeturn is a hack.  Not needed for future
    %object-oriented version!)
    str = sprintf('defaultCurve = %s(''thresholdInitReturn'',true);',curve.sys.functionName);
    eval(str);
    
    %copy the default searchParam, ilow, ihigh values into curve
    if ~isfield(curve,'searchParam')
        curve.searchParam = defaultCurve.searchParam;
    end
    if ~isfield(curve,'ilow')
        curve.ilow = defaultCurve.ilow;
    end
    if ~isfield(curve,'ihigh')
        curve.ihigh = defaultCurve.ihigh;
    end
    if ~isfield(curve,'resolution')
        curve.resolution = defaultCurve.resolution;
    end
    
    %save this curve for future iterations
    curve.sys.thresholdNewCurve = curve;
    
    %set low and high search params
    curve    = setfield(curve,curve.searchParam,curve.ilow);
    curve(2) = curve(1);
    curve(2) = setfield(curve(2),curve(2).searchParam,curve(2).ihigh);
    
    %Validate low returns check = 0
    str = sprintf('thisCurve = %s(curve(1));',curve(1).sys.functionName);
    eval(str);
    [curve,thisCurve] = curveAddMissingNames(curve,thisCurve);
    curve(1) = thisCurve;
    assert(curve(1).check == 0,'low search parameter did not return with check = 0')
    fprintf('Low check passed\n')
    
    %Validate high returns check = 1
    str = sprintf('thisCurve = %s(curve(2));',curve(2).sys.functionName);
    eval(str);
    [curve,thisCurve] = curveAddMissingNames(curve,thisCurve);
    curve(2) = thisCurve;
    assert(curve(2).check == 1,'high search parameter did not return with check = 1')
    fprintf('High check passed\n')

    save(filename,'curve','-v7.3');  %-v7.3 needed for HPCC and some CURVEs?  Why?
end



[low,high] = curveThresholdFindLowHigh(curve);

while (abs(high - low) > curve(1).resolution)
    thisCurve = curve(1).sys.thresholdNewCurve;
    thisCurve.(curve(1).searchParam) = (low + high) / 2;
    str = sprintf('thisCurve = %s(thisCurve);',curve(1).sys.functionName);
    eval(str);
    [curve,thisCurve] = curveAddMissingNames(curve,thisCurve);
    curve(end+1) = thisCurve;
    
    if curve(end).check == 1
        fprintf('%6.4f -- %6.4f    %6.4f\n',low,(low+high)/2,high);
    else
        fprintf('%6.4f    %6.4f -- %6.4f\n',low,(low+high)/2,high);
    end
    
    save(filename,'curve','-v7.3');  %-v7.3 needed for HPCC and some CURVEs?  Why?
    if length(curve) > 100
        error('max number of iterations exceeded');
    end
    
    [low,high] = curveThresholdFindLowHigh(curve);
end
fprintf('saved %s\n',filename);

return

