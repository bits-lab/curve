function status = curveSendMail(toaddr,subject,message)
% curveSendMail - Send an email message
% 
% curveSendMail(toaddr,subject,message) Sends email MESSAGE to TOADDR with
% SUBJECT.  Three arguments are text strings.  This funciton can be used
% for communicating the simulation progress via email.
%
% In this example, curve.continue evaluates false when the simulation has 
% finished.  
%   curve.nWordError = 10;
%   curve.continue   = 'curve.nWordError < 10';
%   if ~eval(curve.continue)
%       msg = sprintf('Finished with %d word errors',curve.nWordError);
%       curveSendMail('user@domain.com',subject,msg);
%   end
%
% Example:
%   Klist = [2 4 8 16 32];
%   k     = 2;
%   subject = sprintf('Simulation is %2.0f%% complete',k / length(Klist));
%   curveSendMail('user@domain.com',subject,'Hello,\nThe subject line
%   contains information about your simulation.\n-Curve Library')
%
% CURVESENDMAIL uses local PHP and its mail() function.
%
% STATUS = CURVESENDMAIL(...)  STATUS is the status code returned by the
% PHP mail function.  0 indicates the mail was accepted, but does not 
% necessarily indicate the mail will reach the destination. If PHP is not 
% installed, then a warning will be generated and the STATUS is 1.
%
% See PHP mail() documentation: http://php.net/manual/en/function.mail.php
%
% Brian Kurkoski
%

localPhp = './mailCurve.php';  %temporary file name for php script

assert(nargin > 1,'A destination email address is required')
assert(nargin < 4,'requires 1, 2 or 3 input arguments')
if nargin < 2
    subject = 'no subject';
end
if nargin < 3
    message  = 'An email from Curve Library';
end

[~,phppath] = system('which php');
if isempty(phppath)
    warning('This system does not have php.  Mail not sent.');
    status = false;
    return;
end

FL = fopen(localPhp,'w');

fprintf(FL,'#!%s\n',phppath);
fprintf(FL,'<?php\n');
fprintf(FL,'$toaddr  = "%s";\n',toaddr);
fprintf(FL,'$subject = "%s";\n',subject);
fprintf(FL,'$message = "%s";\n',message);
fprintf(FL,'$status = mail($toaddr,$subject,wordwrap($message,70));\n');
fprintf(FL,'return $status;\n',phppath);

fclose(FL);
system(sprintf('chmod +x %s',localPhp));
status = system(localPhp);

delete(localPhp);

return
