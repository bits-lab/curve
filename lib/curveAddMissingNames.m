function [curve,crv] = curveAddMissingNames(curve,crv)
%add empty fields to CURVE which exist in CRV but not CURVE. 

FN1      = fieldnames(crv);
FN2      = fieldnames(curve);
newNames = setdiff(FN1,FN2);
for ii = 1:length(newNames)
    t=cell(size(curve)); 
    [curve(:).(newNames{ii})] = deal(t{:});
end

FN1      = fieldnames(curve);
FN2      = fieldnames(crv);
newNames = setdiff(FN1,FN2);
for ii = 1:length(newNames)
    t=cell(size(crv)); 
    [crv(:).(newNames{ii})] = deal(t{:});
end

curve = orderfields(curve,crv);