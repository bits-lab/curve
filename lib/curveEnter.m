function curve = curveEnter(functionName,args)
%CURVEENTER  Initialization for the simulation funciton
%
%  Provides initializaiton for the a simulation funciton in the curve library.
%
%  In typical use, the top of your simulation funciton looks like this:
%
%    FUNCTION MYSIMULATION(VARARGIN)
%
%      CURVE = CURVEENTER(MFILENAME,VARARGIN)
%
%  where MFILENAME is the Matlab function that provides your simulation
%  function name (the string 'MYSIMULATION' in this example).
%
%  VARARGIN is the argument to the simulation function name, and are passed
%  directly to CURVEENTER.  VARARGIN may have two forms: (1) a CURVE
%  structure, or (2) field-value pairs.   For example, the following two
%  are equivalent:
%
%  %(1) providing arguments via curve:
%  curve.Pe = 0.1;
%  curve.n  = 7;
%  curve    = mySimulation(curve);
%
%  %(2) providing arguments as field values pairs:
%  curve    = mySimulation('Pe',0.1,'n',7);
%
%  Form (1) is normally used.  Form (2) is for debugging purposes.
%
%  If 'workInterval' is a field of CURVE, then this value is moved to
%  CURVE.SYS.WORKINTERVAL, and removed from curve.  This allows setting
%  the work interval as:
%
%    mySimulation(...,'workInterval',60)
%
%  (c) Brian Kurkoski
%  Distributed under an MIT-like license; see the file LICENSE


if length(args) == 0
    %CURVE = CURVEENTER;
    curve = curveInit;
    
elseif isstruct(args{1})
    %CURVE = CURVEENTER(CURVE)
    curve = curveInit(args{1});
    
elseif ischar(args{1})
    %CURVE = CURVEENTER('FIELD1',VALUE1,'FIELD2',VALUE2)
    curve = curveInit;
    curve = curveProcessArgs(curve,args{:});
else
    error('input argument error');
end

curve.sys.startTime = clock;
curve.sys.functionName = functionName;

%override default workInterval = 1200 set in curveInit
if isfield(curve,'workInterval')
    curve.sys.workInterval = curve.workInterval;
    curve = rmfield(curve,'workInterval');
end

%override default maxWorkers = 1 set in curveInit
if isfield(curve,'maxWorkers')
    curve.sys.maxWorkers = curve.maxWorkers;
    curve = rmfield(curve,'maxWorkers');
end



%parallel setup
if curve.sys.maxWorkers < 1
    curve.sys.maxWorkers = 1;
end

if curve.sys.maxWorkers > 1
    if isempty(gcp('nocreate'));
        parallelPool = parpool([1,curve.sys.maxWorkers]);
        fprintf('created parallel pool\n');
    else
        parallelPool = gcp;
        fprintf('opened existing parallel pool\n');
    end
    curve.sys.parallelPool    = parallelPool;
    curve.sys.numberOfWorkers = parallelPool.NumWorkers;
else
    curve.sys.parallelPool = [];
    curve.sys.numberOfWorkers = 1;
end

%duplicate the curve for parallel simulations
curve(1:curve.sys.numberOfWorkers) = curve;

return;


function curveArray = curveEnterParallel(curve)
%[parallelPool, sys, curveArray] = curveBeginParallel(sys,curve,maxWorkers,Parallel)
%
%  Preparation for parallel processing simulation
%
%  See also curveCombineParallel
%
%  Brian Kurkoski, 2015

curve.sys.maxWorkers

