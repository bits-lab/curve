function [low,high] = curveThresholdFindLowHigh(curve)

check = [curve.check];
param = [curve.(curve(1).searchParam)];
[param,idx] = sort(param);
check = check(idx);
thr = find(diff(check) == 1);
assert(length(thr) == 1,'previous threshold search results are not monotonic');
low = param(thr);
high = param(thr+1);

return