function curveFromRemote
%curveFromRemote - Copy files from server to local machine
%
%   This function requires configuration variables in PBS/CONFIGUREREMOTE.m
%   are set properly.
%
%   On Mac and Linux systems, CURVEFROMREMOTE downloads files from the remote
%   server to the local machine.
%
%   This command is not supported on Windows system. Copy files manually.



[config,result,pbsdir,configurationFileName] = curveRemoteLoadConfiguration;

if ~result
    fprintf('Failed to load configuration file: %s/%s must be a configuration file\n',pbsdir,configurationFileName);
    return
end




%rsync files from host
if ~ispc
    fprintf('Rsync files from %s to local machine\n',upper(config.servername));
    %cmd    = sprintf('rsync -ave ssh . %s@%s:%s',config.username,config.servername,config.remoteDir);
    cmd    = sprintf('rsync -ave ssh --update %s@%s:%s/ .',config.username,config.servername,config.remoteDir);
    status = system(cmd);
    if status
        error('rsync from server failed');
    end
else
    fprintf('WINDOWS: Copy files from server manually.\n')
end


return



