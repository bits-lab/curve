function curve = curveProcessArgs(curve,varargin)

args = varargin;
if length(args) > 1
    c = true;
else
    error('at least two inputs are required');
end

while (c)
    curve = curveDefaultValue(curve,args{1},args{2});
    args = args(3:end);
    if length(args) == 0
        c = false;
    elseif length(args) == 1
        error('an even number of field-value pairs is required')
    end    
end


