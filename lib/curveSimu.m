function curveSimu(filename)
% CURVESIMU Runs a simulation
%
%  CURVESIMU('filename') loads a system defintion given by FILENAME, and
%  then simulates the system given by the matrix with the
%  name FILENAME.
%
%  CURVESIMU('filename','functionname','name',parameter,...)
%  Creates save a new simulation to FILENAME,
%  sets SYS.FUNCT = FUNCTIONNAME, and places PARAMETERS in the CURVE
%  struct NAME.  PARAMETER may be a cell array, which creates an
%  array of CURVE structres, a single string or a scalar.  
%
%  Brian Kurkoski 12/2005
%

if ~exist([filename '.mat'],'file')
    error('ERROR: file %s.mat does not exist',filename);
end

load(filename)


if ~exist(curve(1).sys.functionName,'file')
    error('Function %s does not exist', upper(curve(1).sys.functionName));
end

if length(curve) == 0;
    fprintf('CURVE is empty, nothing to do\n');
    done = true;
end

if ndims(curve) > 2;
    error('multidimensional CURVE is not supported');
end

done = false;
while(~done)
    
    %Loop over the whole CURVE structure array.  Only if it passes over the
    %whole CURVE array without simulating, then we can declare the whole
    %simulation is done.
    done = true;
    for ii = 1:size(curve,1)
        for jj = 1:size(curve,2)
            disp('*************************************************');
            if (curveErrorLim(curve(ii,jj),true))
                done = false;
                
                %str = sprintf('[sys,crv] = %s(sys,curve(%d));',sys.funct,si);
                str = sprintf('thisCurve = %s(curve(%d,%d));',curve(ii,jj).sys.functionName,ii,jj);
                fprintf('curveSimu(): %s  (filename %s)\n',str,filename);
                
                eval(str);   %thisCurve = mySimulation(curve(2,3)); for example
                [curve,thisCurve] = curveAddMissingNames(curve,thisCurve);
                curve(ii,jj) = thisCurve;
                save(filename,'curve','-v7.3');  %-v7.3 needed for HPCC and some CURVEs?  Why?
                fprintf('saved %s\n',filename);
            end
            
        end
    end
    
end

