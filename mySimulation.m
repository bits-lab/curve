function curve = mySimulation(varargin)

curve = curveEnter(mfilename,varargin);

%Input parameters: default values
curve = curveDefaultValue(curve,'Pe',0.1);
curve = curveDefaultValue(curve,'n',7);

%Output parameters: initalize to 0
curve = curveDefaultValue(curve,'nBitError',0);
curve = curveDefaultValue(curve,'nWordError',0);
curve = curveDefaultValue(curve,'nWords',0);

%Stopping condition: stop when 10 word errors accummulated
curve = curveDefaultValue(curve,'continue','curve.nWordError <10');

%setup: create Hamming Code object
n  = curve.n;
HC = hammingCode(n);

%SIMULATION CORE
while (curveErrorLim(curve))
    %encoder
    u = randi(2,HC.k,1) - 1 ;
    x = mod(HC.G' * u, 2);
    
    %noise
    z = (rand(n,1) < curve.Pe) + 0;
    y = mod(x+z,2);
    
    %decoder
    xhat = HC.decode(y);
    
    %count and accumulate errors
    ne = length(find(xhat ~= x));
    curve.nBitError = curve.nBitError + ne;
    curve.nWordError = curve.nWordError + (ne > 0);
    curve.nWords = curve.nWords + 1;    
end

curve = curveLeave(curve);

%Convenience calculations
curve.ber = curve.nBitError / (curve.nWords * curve.n);
curve.wer = curve.nWordError / curve.nWords ;




