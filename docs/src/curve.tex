\documentclass[10pt,a4paper]{article}

\usepackage{xspace}
\usepackage[pdftex]{hyperref}

%MATLAB
\usepackage[framed,numbered,autolinebreaks,useliterate]{matlab-prettifier}
\usepackage[T1]{fontenc}  %needed for matlab-prettifier nice font
\lstset{style=Matlab-editor,basicstyle=\mlttfamily}  %defaults
%\lstinputlisting{/Users/kurkoski/pr/bpwb/arimoto/capacityComputationForLectureNotesNoDelete.m}
% \begin{lstlisting}  \end{lstlisting}

\newcommand{\curve}{\texttt{curve}\xspace}
\newcommand{\curvelibrary}{Curve Library\xspace}
\renewcommand{\tt}[1]{\texttt{#1}}

\title{\curvelibrary}
\author{Brian M.~Kurkoski}

\begin{document}

\maketitle

\curvelibrary is a Matlab library to enable efficient simulations of communication systems, particularly generating a ``curve'' of  probability of decoder error versus signal-to-noise ratio.  A goal is to obtain statistically reliable data with efficient use of computer time.  If your simulation is implemented using the program interface, then Curve Library is used to begin a simulation, and to stop the simulation when a specified number of errors have occurred, to efficiently use computer time and get reliable results with reasonable amount of computer time. \curvelibrary supports \tt{parfor} in the Matlab parallel processing toolbox, and remote servers, including clusters using PBS.  \curvelibrary allows safely resuming simulations after a server crash.

\section{Introduction}

An error-rate curve shows the decoder error probability versus the signal-to-noise ratio (SNR), and it is a fundamental way to describe the performance of a communication system.  These are obtained by Monte Carlo computer simulations. When generating an error-rate curve, there is a tradeoff between the statistical reliability of your data, and the amount of computer time required to obtain the data. \curvelibrary manages this tradeoff using two techniques:
\begin{enumerate}
\item At each SNR value, the simulation terminates when a stopping condition is reached.  Usually the stopping condition is that a fixed number of word errors have occurred.  This stopping condition has the effect of giving good statistical reliability over a range of SNRs.   

\item  Over an SNR range,  \curvelibrary  spends a roughly equal amount of time simulating all points in an SNR range that have not yet terminated at their stopping condition.   This is particularly useful in preliminary simulations, when you do not know if your simulation will produce good results, or want to find the interesting SNR range.
\end{enumerate}
This is a practical way to efficiently produce SNR/error rate curves. By following the specified interface, \curvelibrary can help manage simulations.

At low SNRs, the decoder error rate is high, the simulation will terminate quickly as the fixed number of errors is quickly accumulated.  At higher SNRs, the simulation continues until enough errors have accumulated to give the desired statistical reliability.  The statistical reliability is connected with the number of word errors.  The word error limit depends on the application, but typically 10 word errors is for preliminary data, and 100 to 500 word errors is needed for publication-quality data.  

To obtain statistical reliability which is high and uniform over a range of SNRs, the number of word errors is a good choice as a stopping condition.   \curvelibrary spends a fixed amount of time at each SNR point.   When setting up a simulation, the SNR values is specified, for example, 0 dB, 1dB, 2 dB, 3 dB.  By default, the simulation works one value for 20 minutes, and then works on the next value for 20 minutes, and so on.  After the last value on the list, it returns to the beginning of the list, continuing cyclically.   If the stopping condition is reached, e.g. if enough word errors have accumulated, this SNR point is considered done, and is dropped from the cycle.

This document assumes a decoder error rate vs. SNR is the objective.  However, other parameters are possible.  A future version of the library may include a noise threshold search.

\curvelibrary is partitioned into four levels, of increasing functionality:
\begin{description}
\item[Level 1] Program interface:  interface for passing parameters to your simulation.
\item[Level 2] Simulation level: efficiently running a simulation over a range of SNRs.
\item[Level 3] Parallel simulations: implement a simulation on a parallel processor.
\item[Level 4] Remote server: run a simulation on a remote server.
\end{description}

The program interface is not very useful by itself, but it implements the necessary interface between your simulation and \curvelibrary.  It is required by all other levels.  The simulation level is useful on single-processor computers, and is required for the other two higher levels.  The parallel simulation level is used when a local parallel processing computer is available.   Remote server level is used when a server is available, with or without parallel processing.

\section{Requirements, Installation and Examples}

\emph{System requirements} \curvelibrary works with a variety of versions of Matlab.  For parallel processing using \tt{parfor} on multicore systems, the Parallel Computing Toolbox is required.   For using a remote server, local and remote versions of ssh and rsync are needed; since the development was done using a local Mac OS X and a remote Linux machine, this scenario is likely to work best.  For local Windows, there is preliminary support for putty for remote execution, but no support for copying files.

\emph{Installation}  Clone the Gitlab repository:
\begin{lstlisting}
$ git clone https://gitlab.com/bits-lab/curve.git 
\end{lstlisting}
or download from \tt{https://gitlab.com/bits-lab/curve}.
% Unzip the distribution file \tt{curve-N.zip} where \tt{N} is the version number. 
In Matlab, change to this directory and run the startup command to add the library \tt{curve/lib} to the search path:
\begin{lstlisting}
>> cd curve                        
>> startup                           %modifies the search path
\end{lstlisting}
 
Below are some examples of \curvelibrary, which can be run after installing the library, except for the remote server example, which requires additional setup.

\emph{Example}  A simple Hamming code simulation function is \tt{mySimulation}.  Using the default parameters, set up a simulation using \tt{curveGenerate} with the file \tt{resultsHamming.mat}.  Then, perform the simulation using \tt{curveSimu}.  After the simulation completes, load the results file, and inspect its contents:
\begin{lstlisting}
>> curveGenerate('resultsHamming','mySimulation') %setup simulation
>> curveSimu('resultsHamming')                  %perform simulation
>> load resultsHamming.mat        %after sim is complete, load file
>> curve.ber                   %results in a structure called curve

ans =

    0.0612                                   %your result will vary
\end{lstlisting}
Setup the simulation using $E_b /N_0 = 4$ dB instead of the default value:
\begin{lstlisting}
>> curveGenerate('resultsHamming','mySimulation','ebnodb',4) 
>> curveSimu('resultsHamming')
\end{lstlisting}
Modify the above example to  perform the simulation over a range of $E_b/N_0$ values:
\begin{lstlisting}
>> curveGenerate('resultsHamming','mySimulation',...
       'ebnodb',{1 2 3 4 5})
>> curveSimu('resultsHamming')
\end{lstlisting}
\emph{Parallel Processing Example}  Run on a local parallel processing machine with 12 cores (the Matlab Parallel Computing Toolbox is required).  With a parallel simulation function \tt{myParallelSimulation} a parallel simulation is specified by setting the \tt{maxWorkers} parameter to be greater than 1:
\begin{lstlisting}
>> curveGenerate('resultsHamming','myParallelSimulation',          'maxWorkers',12); 
>> curveSimu('resultsHamming')
\end{lstlisting}
\emph{Remote Server Example}  Assuming correct configuration (described in Section \ref{sec:remote}), the \tt{curveSimuRemote} library function copies files to a remote server, and executes the simulation. PBS is parallel computing job management software is supported.
\begin{lstlisting}
>> curveGenerate('resultsHamming','myParallelSimulation',...
            'continue', 'curve.nWordErrors < 100',...
            'ebnodb',{0 1 2 3 4},...
            'maxWorkers',12); 
>> curveSimuRemote('resultsHamming'); %run sim on remote server
\end{lstlisting}

\section{Level 1: Program Interface}

If you write your simulation using the \curvelibrary program interface described in this section, it allows your simulation to be managed by higher-level functions described in the following sections.  The program interface handles the input parameters and output results to and from your simulation, as well as terminating the simulation with a stopping condition.  A key point of the interface is that communication uses a structure called \curve.  The inputs and outputs to your simulation are fields in this structure.  An example of a simulation function that implements the program interface is on page \pageref{fig:sim}.

Using the interface, your simulation can be called as follows. Suppose you have written a function called \tt{mySimulation} which implements your simulation. The input arguments are a series of field-value pairs, for example:
\begin{lstlisting}
>> curve = mySimulation('n',7,'ebnodb',2); 
\end{lstlisting}
%or equivalently with a \curve structure:
%\begin{lstlisting}
%>> curve.n      = 7;
%>> curve.ebnodb = 1;
%>> curve        = mySimulation(curve);
%\end{lstlisting}
In order to implement the program interface, your simulation function \tt{mySimulation.m} should begin something like:
\begin{lstlisting}
function curve = mySimulation(varargin)

curve = curveEnter(mfilename,varargin);
\end{lstlisting}
Here \texttt{curveEnter} parses input arguments and performs other setup operations.  Note that \tt{mfilename} is a built-in Matlab function that returns the value of the current function, which is the string \tt{'mySimulation'} in this case.

All input parameters are optional, so the function must have default values specified, for example, the parameter \tt{ebnodb} has a default value 1:
\begin{lstlisting}
curve = curveDefaultValue(curve,'ebnodb',1); 
\end{lstlisting}
If \curve does not contain the field, or if the value is empty, then \tt{curveDefaultValue} sets the field to the default value.   If \curve already contains a non-empty field, then it is not changed.  All fields used as inputs and outputs should have default values set in this way.  Outputs should also be set to some default value, for example a counter for the number of word errors is set to 0:
\begin{lstlisting}
curve = curveDefaultValue(curve,'nWordErrors',0); 
\end{lstlisting}
In particular, you should not use \tt{curve.nWordErrors = 0}, since this will always reset this field to 0.  \curvelibrary is robust so that even if the simulation has not yet finished, your simulation can continue by calling the simulation function again, using the data obtained so far.  In this way, if an output parameter such as \tt{nWordErrors} already has a value, this value is not changed by \tt{curveDefaultValue}.

Your simulation will continue until a stopping condition is satisfied.  The stopping condition is specified as a string containing Matlab code, in the field \texttt{curve.continue}.   Code in this string must evaluate \tt{true} or 1 if the stopping condition is not satisfied, \tt{false} or 0 if the stopping condition is satisfied.  For example, if the stopping condition is that 10 word errors have occurred, then the ``continue condition'' is:
\begin{lstlisting}
curve = curveParam(curve,'continue','curve.nWordError < 10'); 
\end{lstlisting}
The string \texttt{'curve.nWordError < 10'} is an example of Matlab code that evaluates either \tt{true} or \tt{false}.   

The core of your simulation is executed many times, repeating the encoding and decoding, each time using randomly generated data sequences and noise.  This simulation core should be inside of a \tt{while} loop as follows:
\begin{lstlisting}
while (curveErrorLim(curve))                      %SIMULATION CORE
    %perform a simulation...
end
\end{lstlisting}
Before attempting each loop, \tt{curveErrorLim} will evaluate \tt{curve.condition} and stop the simulation if it has been satisfied.  The simulation can also stop temporarily if a time limit has been reached, see the next section.

During your simulation, output parameters are field elements, and they can be directly modified, for example:
\begin{lstlisting}
curve.nWords = curve.nWords + 1; 
\end{lstlisting}

Before your simulation function exits, call the \tt{curveLeave} function, which records the elapsed time, and also handles parallel processing operations:
\begin{lstlisting}
curve = curveLeave(curve);
\end{lstlisting}

A complete example is in Fig.~\ref{fig:sim} on page \pageref{fig:sim}.

\begin{figure}
 \lstinputlisting{../../mySimulation.m}
 \caption{Example of a simulation function. \label{fig:sim}}
\end{figure}


\section{Level 2: Simulations}

\curvelibrary handles simulations over an SNR range, and initially allocates a equal amount of processor time to each value in the SNR range.  As the simulation progresses, high error rate points reach their stopping condition sooner, and \curvelibrary allocates processor time only to those points which have not yet reached their stopping condition.  This is effective for initial simulations when the SNR range of interest is not known.  It aims to get uniform statistical reliability over the SNR range.  It uses processor time effectively within the restriction of the desired statistical reliability. 

Data is stored in an array of \curve structures, with one structure for each SNR value.  For example, the SNR values 0 dB, 1 dB, 2 dB, 3 dB are stored in a 1-by-4 array of \curve array structures. This structure array is stored in a .mat file.   When the simulation begins, this file is read.  The data in the file is used to perform the simulation.  Data is saved to this file periodically.  In the event of a server crash, data from the last save is retained.

The \tt{workInterval} parameter sets the amount of computer time to spend on one SNR point, before stopping and moving to the next SNR point.  The default value of the \tt{workInterveral} parameter is 1200 seconds (20 minutes). The simulation begins by starting your simulation function with SNR of 0 dB, and continues until one of two conditions is reached: (1) the stopping condition in \tt{continue} is reached, or (2) \tt{workInterval} seconds have passed.  The results are saved.   Then, the simulation moves to 1 dB, and simulates until the stopping condition or working time is reached, and again saves the data.  Then 2 dB, then 3 dB.  After simulating 3 dB, a maximum of 80 minutes has passed, less if a stopping condition was reached.  Then, the simulation returns to 0 dB.  If the stopping condition for 0 dB is already satisfied, then the 0 dB simulation is not performed, and moves to the 1 dB simulation.   This continues cyclicly until all the stopping conditions for the four points are satisfied. 

As an example, a simulation for the mySimulation Hamming code would be set up as follows:
\begin{lstlisting}
>> curveGenerate('resultsHamming','mySimulation','ebnodb',{0 1 2 3},'n',7);
\end{lstlisting}
This saves data in the file \tt{resultsHamming.mat}, which contains the \curve structure array.  The simulation function is \tt{mySimulation.m}.  A \curve structure array with four elements is created, where the \tt{ebnodb} field is set to 0, 1, 2 and 3 in each.  Another field \tt n is set to 7 for each structure of the array.

The simulation is performed using:
\begin{lstlisting}
>> curveSimu('resultsHamming')
\end{lstlisting}
The file \tt{resultsHamming.mat} contains the data for the simulation.   This file is loaded before the simulation begins, and the results are saved to this file, both periodically as the simulation progresses and at the end.  

If the server crashes, then \texttt{curveSimu('resultsHamming')} will resume the simulation from the point of the last periodically saved file.

After the simulation is complete, or even while it is running, data is available in the file.  For example, plot the results using:
\begin{lstlisting}
>> load resultsHamming.mat
>> semilogy([curve.ebnodb],[curve.ber])
\end{lstlisting}

\section{Level 3: Parallel Processing}

It is relatively easy to perform communications simulations using parallel processing, because each processor runs the same simulation, except that the random number generator seed differs.  There are several points to be careful about when implementing a parallel simulation, including writing parallel code that Matlab will accept.  Another point to be careful about is combining the results from multiple processors after the simulation is complete, and \curvelibrary provides support for this.

The parameter \tt{maxWorkers} controls the number of parallel workers (processor cores) that will be requested.  If \tt{maxWorkers} is 1, then the simulation is non-parallel, this is the default.  Set \tt{maxWorkers} to be greater than 1 to enable parallel simulations:
\begin{lstlisting}
>> curveGenerate('resultsHamming_n7','myParallelSimulation',       'maxWorkers',12); 
>> curveSimu('resultsHamming')
\end{lstlisting}

For parallel simulations, the \tt{curveEnter} function
%returns a \curve structure array with elements equal to the number of workers:
\begin{lstlisting}
curve = curveEnter(mfilename,varargin);
\end{lstlisting}
returns a structure array \tt{curve} of size equal to the number of workers.  The number of workers depends on the server environment and is less than or equal to \tt{maxWorkers}, and its value is \tt{curve(1).sys.numberOfWorkers}.

It is necessary to specify how the various fields will be combined when the parfor loop is compete.  If the field values should be added after the parfor loop is compete (for example, the number of words, the number of errors), it is specified using the \tt{'add'} option for \tt{curveDefaultValue}:
\begin{lstlisting}
curve = curveDefaultValue(curve,'nWords',0,'add');
\end{lstlisting}
If the fields are to be concatenated, for example to store the random number generator state after failed decoding, it is specified using the \tt{'concat'} option
\begin{lstlisting}
curve = curveDefaultValue(curve,'rngstate',{},'concat'); 
\end{lstlisting}
For values which do not change during the parfor loop you do not need to specify a combining method (\curvelibrary copies the value from \tt{curve(1)}).


Your simulation core should \tt{parfor} loop over the \tt{curve} array:
\begin{lstlisting}
%SIMULATION CORE
parfor worker = 1:length(curve);
    while (curveErrorLim( curve(worker) ))
        %simulation...
        %inside of parfor, must use CURVE(WORKER) and not CURVE 
    end
end 
\end{lstlisting}
You must take some care in writing your parfor loop, because Matlab has restrictions on variable slicing and parallel processing.  You should always reference \tt{curve(worker)}  and not \tt{curve}, for example.  Read ``\href{http://www.mathworks.com/help/distcomp/classification-of-variables-in-parfor-loops.html}{Classification of Variables in Parfor Loops}'' in the Matlab documentation, and FAQs in Section \ref{sec:faq}.

After the parfor loop is compete, the \curve array structure is combined into a single structure.  Combining is performed by \tt{curveLeave}, which has a single \curve structure as output.  Note that convenience calculations like the bit-error rate, should be performed after \tt{curveLeave}:
\begin{lstlisting}
curve     = curveLeave(curve);
curve.ber = curve.nBitError / (curve.nWords * curve.n);
curve.wer = curve.nWordError / curve.nWords ; 
\end{lstlisting}

A complete example of a simulation example for parallel processing is in Fig.~\ref{fig:para}.

\begin{figure}
\lstinputlisting{../../myParallelSimulation.m}
\caption{Example of simulation function for parallel processing. \label{fig:para} }
\end{figure}

\newpage

\section{Level 4: Remote Server \label{sec:remote}}

Using the Matlab command line, you can run your simulation on a remote server. Support for PBS multiprocessor management is included.  After setting up necessary configuration, the command:
\begin{lstlisting}
>> curveGenerate('resultsHamming','myParallelSimulation',          'maxWorkers',12); 
>> curveSimuRemote('resultsHamming')  
\end{lstlisting}
will copy files to the server, and start the simulation on the server.  After your simulation is completed, copy the files back to the local machine, and inspect the results.
\begin{lstlisting}
>> curveFromRemote
>> load resultsHamming.mat
>> curve.ber 

ans =

    0.0598                                   %your result will vary
\end{lstlisting}

On Mac and Linux systems, \curvelibrary uses rsync to copy files and ssh to execute commands on the remote server.   On Windows, these commands are not available by default ---  putty is used to execute commands on the remote server, but files must be copied manually.

\curvelibrary must be configured correctly.  It is necessary to (1) specify  a working directory tree and (2) setup a configuration file.  The \curvelibrary must be on the remote machine, so it is necessary to (3) manage libraries to be copied to the remote server. It is strongly recommended that you (4) configure keychain on your local machine to avoid entering passwords. 

(1) Specify a working directory.  The working directory contains only files related to your simulation.  Using rsync, this directory and all its subdirectories are copied to the server by the \tt{curveSimuRemote} command.  On the server side, Matlab will be started in the working directory.  

(2) The directory \tt{var/} must be in the working directory, and it must contain the configuration file \tt{var/configureRemote.m}.  The configure file included in the distribution is shown in Figure \ref{fig:configRemote}. Modify these settings for your server environment.  

(3) Manage your libraries.  The \curvelibrary needs to be copied to the server, your simulation file may require other libraries to be the server.  These libraries should on the Matlab search path.  A simple approach is to place the \tt{lib/} directory (and your own libraries) in the working directory and to put a \tt{startup.m} file:
\begin{lstlisting}
addpath([pwd '/lib']);         %curve library
addpath([pwd '/myLib']);       %your library
\end{lstlisting}
in the working directory. When Matlab starts in the working directory on the remote server, it will execute \tt{startup.m}, and add the \tt{curve/var} directory to the Matlab search path.

\begin{figure}[t]
 \lstinputlisting{../../var/configureRemote.m}
 \caption{\tt{configureRemote.m} as included in the distribution.  Must be modified for your server environment.  \label{fig:configRemote}}
\end{figure}

Alternatively, Matlab will execute the default \tt{startup.m} file, which may be in a different location.   The location depends on the server operating system and other settings, read  \href{http://www.mathworks.com/help/matlab/matlab_env/matlab-startup-folder.html}{``Matlab Startup Folder''} for more information.  A local shell command specified by \tt{localShellCommand} will be executed before copying files to the server, for example to copy local files into the working directory.

Your working directory might be \tt{\textasciitilde/myProject}, and it might contain the following files and directories:
\begin{verbatim}
var/                    %curve remove configuration directory 
var/configureRemote.m   %remote configuration 
startup.m               %sets library path
lib/                    %curve library libraries 
myLib/                  %your libraries 
mySimulation.m          %your simulation file
myData.mat              %your saved simulation data
\end{verbatim}

(4) \emph{Keychain}  Use keychain to avoid typing passwords when you connect to the server.  Using public key cryptography, your local machine has a private key, and the server has the public key.  To set up keychain on Mac OS X and Linux, perform the following on the \tt{local} machine and \tt{remote} machine:
\begin{verbatim}
local$ ssh-keygen 
\end{verbatim}
Accept the default location.  To use no passphase, press enter 2 times.  
\begin{verbatim}
local$ scp .ssh/id_rsa.pub s1510000@hpcc:~/.ssh 
local$ ssh s1510000@hpcc
remote% cd .ssh
remote% cat id_rsa.pub >> authorized_keys2
remote% chmod 600 authorized_keys2
\end{verbatim}
In this example, the username is \tt{s1510000} and the server name is \tt{hpcc}. By installing keychain with no passphrase, unauthorized access to your local machine might allow unauthorized access to the server. Additional software like keychain or Keychain Access can help manage passphrases.

In addition, \curvelibrary provides the following commands.

\begin{description}
\item[\tt{curveRemoteShell}] runs a shell command on the server in the working directory.   For example, \tt{curveRemoteShell('qstat -H 128674')} displays the status of PBS job 128674.   A simple check of your configuration setting is \tt{curveRemoteShell('pwd')}, which should return the working directory on the server.  Stop and delete job 128674 with \tt{curveRemoteShell('qdel 128674')}.  List all \tt{username} jobs with \tt{curveRemoteShell('qstat -u username')}.

\item[\tt{curveFromServer}] uses rsync to copy files from the server to the local machine.  This is for copying simulation results stored in files back to the local machine.  The rsync \tt{--update} option is used so that only files that have been changed, for example simulation results, will be copied.

\item[\tt{curveSendMail}] can send you an email message, for example when your simulation completes.

\end{description}

\section{Threshold Search}

Preliminary support has been added for threshold searches, for example, the noise threshold for LDPC codes.  The function interface is similar, making threshold searches straightforward, although there is no parallel processing or statistical reliablity aspects.  The threshold search performs a simple binary search, and your function must provide a 0/1 or \tt{false}/\tt{true} value in \tt{curve.check} to indicate if the result is below or above the threshold.

In your \tt{myThresholdSearch} file, the first three lines should be of the form:
\begin{lstlisting}
curve = curveEnter(mfilename,varargin);
[curve,thresholdInitReturn] ...
             = curveEnterThreshold(curve,searchParam,low,high,res);
if thresholdInitReturn; return; end 
\end{lstlisting}
The first line is as before.  In the second line, \tt{curveEnterThreshold} has five required parameters.  After \tt{curve},  \tt{searchParam} is parameter that is being searched, and it must be a field.  \tt{low} and \tt{high} are the initial low and high search points.  \tt{res} is the resolution that must be achieved, that is, the threshold search will stop when the gap between high and low is less than \tt{res}.  For example,
\begin{lstlisting}
[curve,thresholdInitReturn] ...
                  = curveEnterThreshold(curve,'SNRdb',0,2,0.05);
\end{lstlisting}
shows the parameter search is \tt{curve.SNRdb}, the initial low value is 0, the initial high value is 2, and the resolution is 0.05.  

Your function must set the value \tt{curve.check} to 0 or \tt{false} when the search parameter is equal to \tt{low}.  And, it must set  the value \tt{curve.check} to 1 or \tt{true} when the search parameter is equal to \tt{high}.  A simple strategy is to set extreme inital values, since the search is binary.

To perform a search, setup the threshold search then run \tt{curveThresholdSearch}.  For example, if your function is \tt{myThresholdSearch} and the data file is \tt{threshData}, and the parameter \tt{Ns} should be 10000 then:
\begin{lstlisting}
curveGenerate('threshData','myThresholdSearch','Ns',10000);
curveThresholdSearch('threshData');
\end{lstlisting}
Once the threshold search is complete:
\begin{lstlisting}
load threshData
[low,high] = curveThresholdFindLowHigh(curve) 
\end{lstlisting}
will give the \tt{low} and \tt{high} limits of the search.

\section{Examples \label{sec:examples} }

This section gives examples of \curvelibrary.

\emph{Example} Perform a simulation with a stopping condition of 10 word errors.  After the simulation is complete, the results look promising, but you want to increase the statistical reliability by increasing to 100 word errors.  Update all \tt{continue} fields at the same time:
\begin{lstlisting}
>> curveGenerate('resultsHamming','mySimulation',...
       'ebnodb',{1 2 3 4},'continue','curve.nWordError < 10')
>> curveSimu('resultsHamming')
>> load resultsHamming.mat
>> [curve(:).continue] = deal('curve.nWordError < 100') 
>> save resultsHamming curve
>> curveSimu('resultsHamming')
\end{lstlisting}

\emph{Example} After 24 hours of simulation time, SNRs of 4 dB and 5 dB gave no errors at all.   These should be removed from the simulation so that the server can spend more time on SNRs of 0 dB, 1 dB, 2 dB and 3 dB.  Assuming \curve array has 6 elements:
\begin{lstlisting}
load myData.mat
curve = curve(1:4);
save myData curve
curveSimu('myData')
\end{lstlisting}
Or:
\begin{lstlisting}
load myData.mat
curve(5).continue = false;
curve(6).continue = false;
save myData curve
curveSimu('myData')
\end{lstlisting}
The first form deletes the data, the second form keeps the data but will never be attempted since the continue condition is false.

\section{FAQ \label{sec:faq}}

\begin{description}
\item[Q] I got the error message ``Too many input arguments'' even though the number of input arguments is correct.  The error occurs at a function I wrote.
\item[A]  When parallel processing, elements of structures cannot be passed directly as input arguments, that is, \tt{var = ebnodb2var(curve.ebnodb)} generates the error.  The solution is to pass the value using a new variable:
\begin{lstlisting}
ebnodb = curve.ebnodb;
var    = HC.ebnodb2var(ebnodb);
\end{lstlisting}
\item[Q] I got the message ``the variable \tt{curve} in \tt{parfor} cannot be classified''.  
\item[A] This might mean you need to change \tt{curve} to \tt{curve(worker)}.
\end{description}


\section{Reference}

\curve contains two predefined fields: \tt{continue} which is discussed above, and \tt{sys}, which is itself a structure.  \tt{sys} has fields that are set automatically, or through special functions:
\begin{itemize}
\item  \texttt{sys.startTime}  The time the current simulation began, as indicated by the matlab \tt{clock} function.  The default value is empty.  After the simulation finishes, it is set to empty. 
\item  \texttt{sys.elapsedTime} The accumulated time that has been spent on this simulation.   In parallel simulations, the time on all processors is counted only once.
\item  \texttt{sys.functionName}  Your simulation function name, for example \tt{'mySimulation'}.
\item  \texttt{sys.workInterval} Time in seconds to simulate on this \curve, before moving to the next one.  The default value is 1200 seconds (20 minutes).
\item  \texttt{sys.inputs}  When a simulation is initialized using \tt{curveGenerate}, the original input arguments are stored here.  This is useful for starting a new simulation with the same or similar parameters.
\end{itemize}
The following fields deal with parallel processing:
\begin{itemize}
\item  \texttt{sys.maxWorkers} Maximum number of workers requested by the user.  A number greater than 1 implies a parallel simulation. Default value is 1.
\item  \texttt{sys.parallelPool}  Parallel pool provided by \tt{gcp}.  
\item  \texttt{sys.numberOfWorkers}  Number of workers provided by the system, as reported by \tt{gcp}. 
\item  \texttt{sys.addField, sys.concatField}  Cell arrays of strings, indicating the combining rule for each \curve field.
\end{itemize}

You can control if your local machine automatically opens a parallel pool when seeing \tt{parfor}.  In Matlab, go to Preferences, Parallel Computing, and change the option.  On a non-GUI server, there is no documented access to disable.  But there is an undocumented function that appears to disable it:
\begin{lstlisting}
ps = parallel.Settings; 
ps.Pool.AutoCreate = false; 
\end{lstlisting}


\end{document}